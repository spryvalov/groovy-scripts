# Groovy scripts #

### Custom LG inbound file generator
* can be executed with groovy Custom_LG_file.groovy, 
* 'INPUT_FILE' Should be valid LG intake location, with at least 1 member.
* 'OUTPUT_FILE' - output file location
* Script can be configured to add required number of employees with optional dependents

### DOMO recap script
* can be executed with groovy DOMO_recap.groovy, 
* 'TEST_QUERY' pulls top 1 Enrollment and submits to ESB DOMO workflow
* 'WORK_QUERY' pulls and submits all available records in Enrolling or Accepted status
* Script terminates execution after 10 failed records

### Compare XML
* Script executes two synchronous workflows and compares output with XMLUnit Diff
* Can be executed with groovy Compare_xml.groovy 
* RECORDS_TO_PROCESS - number of Closed enrollment guids to pull out of TDS 
* DUMP_FILE_DIR In case diff is found dumps both files to this folder
* OLD_WORKFLOW = 'GroupSetupOld' and NEW_WORKFLOW = 'GroupSetup' - workflow id's to compare output

### Compare CSV 
* Script executes two synchronous workflows and compares output with String.equals(String other)
* Can be executed with groovy Compare_CSV.groovy 
* RECORDS_TO_PROCESS - number of Closed enrollment guids to pull out of TDS 
* DUMP_FILE_DIR In case diff is found dumps both files to this folder
* OLD_WORKFLOW and NEW_WORKFLOW - workflow id's to compare output

### Compare EDI 
* Can be executed with groovy Compare_EDI.groovy 
* RECORDS_TO_PROCESS - number of Accepted quotes script will try to pull out. (Actual can be less) 
* Script pulls 'Accepted' Enr quote IDs from ia-alpha db (hardcoded) and searches for corresponding guid's in the TDS
* Then script executes two synchronous workflows sequentially for all guids, and compares workflow outputs with String.equals(String other)
* As EDI records carry generation timestamp, this diff is being ignored.
* DUMP_FILE_DIR - In case diff is found dumps both files to this folder
* OLD_WORKFLOW and NEW_WORKFLOW - workflow id's to compare output