@Grapes([
        @Grab(group = 'com.github.javafaker', module = 'javafaker', version = '[0.13,)'),
        @Grab(group = 'commons-lang', module = 'commons-lang', version = '[2.5,)')
])
import com.github.javafaker.Faker
import org.apache.commons.lang.RandomStringUtils

import javax.xml.parsers.DocumentBuilderFactory
import javax.xml.transform.OutputKeys
import javax.xml.transform.TransformerFactory
import javax.xml.transform.dom.DOMSource
import javax.xml.transform.stream.StreamResult
import java.text.SimpleDateFormat

class CustomLgIntakeGenerator {

    public static final String INPUT_FILE = "/home/pryvalov/Documents/Life_FTP_OK.xml"
    public static final String OUTPUT_FILE = "/home/pryvalov/Documents/Life_FTP_OK_100ee.xml"

    void generate() {
        def fXmlFile = new File(INPUT_FILE)
        def dbFactory = DocumentBuilderFactory.newInstance()
        def dBuilder = dbFactory.newDocumentBuilder()
        def doc = dBuilder.parse(fXmlFile)
        doc.getDocumentElement().normalize()

        def household = doc.getDocumentElement().getElementsByTagName("Household").item(0).cloneNode(true)
        def employeeMember = household.getElementsByTagName("Member").item(0).cloneNode(true)
        def members = household.getElementsByTagName("Members").item(0)
        removeChilds(members)
        members.appendChild(employeeMember)
        def census = doc.getElementsByTagName("Census").item(0)

        removeChilds(census)
        def sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
        (0..100).each { i ->
            def household1 = household.cloneNode(true)
            def uuid = RandomStringUtils.randomAlphanumeric(15)

            household1.getElementsByTagName("ExternalMemberID").item(0).setTextContent(uuid)
            household1.getElementsByTagName("HouseHoldEmployeeId").item(0).setTextContent(uuid)
            def faker = new Faker()
            def first = faker.name().firstName()
            def last = faker.name().lastName()

            household1.getElementsByTagName("FirstName").item(0).setTextContent(first)
            household1.getElementsByTagName("LastName").item(0).setTextContent(last)
            household1.getElementsByTagName("BirthDate").item(0)
                    .setTextContent(sdf.format(faker.date().between(new GregorianCalendar(1940, 0, 1).getTime(), new GregorianCalendar(1999, 0, 1).getTime())))


            if (false) // condition to add deps
            {
                def depCount = (i < 200) ? 1 : 3 // = ThreadLocalRandom.current().nextInt(0, 4)

                (0..depCount).each
                        { j ->
                            def dependent = employeeMember.cloneNode(true);
                            dependent.getElementsByTagName("Type").item(0).setTextContent(j == 0 && i > 100 ? "Spouse" : "Child")
                            dependent.getElementsByTagName("FirstName").item(0).setTextContent(new Faker().name().firstName())
                            dependent.getElementsByTagName("LastName").item(0).setTextContent(last);
                            dependent.getElementsByTagName("ExternalMemberID").item(0).setTextContent(uuid + "D" + j)
                            if (j == 0 && i > 100) {
                                dependent.getElementsByTagName("BirthDate").item(0)
                                        .setTextContent(sdf.format(faker.date().between(new GregorianCalendar(1940, 0, 1).getTime(), new GregorianCalendar(1999, 0, 1).getTime())))
                            } else {
                                dependent.getElementsByTagName("BirthDate").item(0)
                                        .setTextContent(sdf.format(faker.date().between(new GregorianCalendar(1995, 0, 1).getTime(), new GregorianCalendar(2016, 0, 1).getTime())))
                            }

                            household1.getElementsByTagName("Members").item(0).appendChild(dependent)
                        }
            }

            census.appendChild(household1)
            println i
        }
        def source = new DOMSource(doc)
        def writer = new FileWriter(new File(OUTPUT_FILE))
        def result = new StreamResult(writer)

        def transformerFactory = TransformerFactory.newInstance()
        def transformer = transformerFactory.newTransformer()
        transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8")
        transformer.setOutputProperty(OutputKeys.INDENT, "yes")
        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2")
        transformer.transform(source, result)
    }

    private void removeChilds(node) {
        while (node.hasChildNodes())
            node.removeChild(node.getFirstChild())
    }
}


new CustomLgIntakeGenerator().generate()