import groovy.json.JsonSlurper
import groovy.sql.Sql
import groovy.util.logging.Slf4j

import javax.xml.datatype.DatatypeFactory

@Grapes([
        @Grab(group = 'ch.qos.logback', module = 'logback-classic', version = '[0.9.28,)'),
        @Grab(group = 'com.microsoft.sqlserver', module = 'mssql-jdbc', version = '6.2.1.jre8'),
        @GrabConfig(systemClassLoader = true)
])
@Slf4j
class EdiCompare {


    static final int RECORDS_TO_PROCESS = 2
    static final String DUMP_FILE_DIR = "./log/"
    static final JsonSlurper slurper = new JsonSlurper()
    static final XmlSlurper xmlSlurper = new XmlSlurper()
    public static final String OLD_WORKFLOW = 'Group834Old' //'PINReportOld' or 'GroupCensusReportOld'
    public static final String NEW_WORKFLOW = 'Group834'    //'PINReport-v2' or 'GroupCensusReport'

    public static final DB_URI = 'jdbc:sqlserver://CNXBRKSGQA03.ct.com:49762;databaseName=EXEMPLAR_R7_8_x_QA1_Tran'
    public static final USER = 'universal_login'
    public static final PASSWORD = 'firefly'
    public static final DRIVER = 'com.microsoft.sqlserver.jdbc.SQLServerDriver'
    public static final String TEST_QUERY = """
                    SELECT TOP $RECORDS_TO_PROCESS QT_QUOTE.QUOTE_ID, QT_QUOTE.GROUP_ID FROM QT_QUOTE
                      INNER JOIN QT_WORKFLOW_STATE ON QT_WORKFLOW_STATE.QUOTE_ID = QT_QUOTE.QUOTE_ID
                      INNER JOIN GR_GROUP ON QT_QUOTE.GROUP_ID = GR_GROUP.GROUP_ID
                    WHERE  GR_GROUP.GROUP_XREF_TYPE_ID = 3 AND QT_WORKFLOW_STATE.WORKFLOW_STATE_TYPE_ID = 9
                    ORDER BY QT_QUOTE.QUOTE_ID DESC
                    """


    private int succesfullCount = 0
    private List<String> corruptRecords = []
    private List<Long> oldTimings = []
    private List<Long> newTimings = []

    void compare() {
        try {
            log.info "Establising connection to $DB_URI"
            LinkedHashMap acceptedQuotesMap = findAcceptedPandaQuotes()
            log.info "Found ${acceptedQuotesMap.size()} accepted quotes in the database."

            log.info "Performing TDS elasticsearch for associated records..."
            String searchResponse = submitTdsSearch(acceptedQuotesMap)
            def parsedResponse = new JsonSlurper().parseText(searchResponse)
            log.info "TDS responded with ${parsedResponse.size} results."

            def guids = parsedResponse.values.findAll
            { val -> acceptedQuotesMap.get(val.quoteId) == val.groupId }.collect { it.guid }
            guids.removeAll([null]) //sometimes elasticsearch responds with null guid for some reason
            log.info "Found ${guids.size()} matching GUID records."

            guids.each { performEdiComparison(it) }
        } finally {
            log.info("Successfully matched $succesfullCount records, \n Corrupt record guids: ${corruptRecords}  ")
            log.info("Old wf timings $oldTimings \n with average ${oldTimings.sum() / oldTimings.size()}")
            log.info("New wf timings $newTimings \n with average ${newTimings.sum() / newTimings.size()}")
        }
    }

    private void performEdiComparison(String guid) {
        def start = System.currentTimeMillis()
        def esbResponseOldWf = executeEnrollmentIntegration(guid, '', OLD_WORKFLOW)
        def expected = xmlSlurper.parseText(esbResponseOldWf)
        if (expected.Code != 200) {
            log.error("corrupt record or ESB failure, verify $guid")
            corruptRecords.add(guid)

            return
        }
        def oldWfElapsed = System.currentTimeMillis() - start

        start = System.currentTimeMillis()
        def esbResponseNewWf = executeEnrollmentIntegration(guid, '', NEW_WORKFLOW)
        def actual = xmlSlurper.parseText(esbResponseNewWf)
        if (actual.Code != 200) {
            log.error("corrupt record or ESB failure, verify $guid")
            corruptRecords.add(guid)

            return
        }
        newTimings.add(System.currentTimeMillis() - start)
        oldTimings.add(oldWfElapsed)

        String expected1 = expected.Payload.PayloadContent.Base64.text()
        String actual1 = actual.Payload.PayloadContent.Base64.text()

        expected1 = removeEdiTimestamps(expected1)
        actual1 = removeEdiTimestamps(actual1)


        if (expected1 != actual1) {
            new File("${DUMP_FILE_DIR}expected.edi").write expected1
            new File("${DUMP_FILE_DIR}actual.edi").write actual1

            throw new RuntimeException("Aborting execution due to found diff \n Check files dumped to $DUMP_FILE_DIR")
        } else {
            log.info "Processed $guid no differences found"
            succesfullCount++
        }
    }

    private String removeEdiTimestamps(String rawEdi) {
        rawEdi.replaceAll("\\*2017\\d{4}\\*\\d{6}\\*", "*DUMMY_TIMESTAMP*")
    }

    private String executeEnrollmentIntegration(guid, type, workflowId, carrierId = 'nixdev') {
        def timestamp = DatatypeFactory.newInstance().newXMLGregorianCalendar(new GregorianCalendar() {
            {
                setTime(new Date())
            }
        })
        def postToEsb = new URL("http://10.1.2.37:9000/integration/platform/workflowengine/rest/executeWorkflow").openConnection()
        def esbRequest = """
                <ns2:IntegrationRequest xmlns:ns2="http://www.connecture.com/integration/esb/platform" xmlns="http://www.connecture.com/integration/esb">
                  <InternalServiceRequest>
                    <RequestID>${UUID.randomUUID()}</RequestID>
                    <Version>1.0</Version>
                    <Timestamp>$timestamp</Timestamp>
                    <CarrierId>$carrierId</CarrierId>
                  </InternalServiceRequest>
                  <ns2:WorkflowId>$workflowId</ns2:WorkflowId>
                  <ns2:XmlPayload>
                    <Request xmlns="http://www.connecture.com/sgenrollment">
                     <GUID>$guid</GUID>
                     <type>$type</type>
                    </Request>
                </ns2:XmlPayload>
                </ns2:IntegrationRequest>
        """
        postToEsb.setRequestMethod("POST")
        postToEsb.setDoOutput(true)
        postToEsb.setRequestProperty("Content-Type", "application/xml")
        postToEsb.setRequestProperty("Authorization", "Basic cWFwbGF0Zm9ybXVzZXI6cWFwYXNzd29yZA==")
        postToEsb.getOutputStream().write(esbRequest.getBytes("UTF-8"))
        log.debug("executing $workflowId for $guid")
        def esbResponse = postToEsb.getInputStream().getText()
        log.debug("executed $workflowId for $guid")

        return esbResponse
    }

    private LinkedHashMap findAcceptedPandaQuotes() {
        def acceptedQuotesMap = [:]
        def sql = Sql.newInstance(DB_URI, USER, PASSWORD, DRIVER)

        sql.query(TEST_QUERY) { resultSet ->
            while (resultSet.next()) {
                def quoteId = resultSet.getInt(1)
                def groupId = resultSet.getInt(2)
                acceptedQuotesMap.put(quoteId, groupId)
            }
        }
        sql.close()

        return acceptedQuotesMap
    }

    private String submitTdsSearch(LinkedHashMap acceptedQuotesMap, page = 1) {
        def post = new URL("http://cnxatlsgqa12.ct.com:9007/exemplar/groupEnrollment-v1/search?page=$page").openConnection()
        def message = """
                {
                    "searchQuery": {
                        "query": {
                            "terms":{
                                "quoteData:id.keyword": ${acceptedQuotesMap.keySet()}
                            }
                        }
                    },
                    "responseLocations": {
                        "guid": "guid",
                        "groupId": "groupData.id",
                        "quoteId": "quoteData.id",
                        "type": "groupData.type"
                    }
                }
        """
        post.setRequestMethod("POST")
        post.setDoOutput(true)
        post.setRequestProperty("Content-Type", "application/json")
        post.setRequestProperty("Authorization", "Basic c3lzdGVtVXNlcjozY20ldDdYLWs3NWptQD9r")
        post.getOutputStream().write(message.getBytes("UTF-8"))

        def outputText = post.getInputStream().getText()

        return outputText
    }
}

new EdiCompare().compare()

