/**
 * Script executes two synchronous workflows and compares output with XMLUnit Diff
 * Can be executed with groovy Compare_xml.groovy 
 * RECORDS_TO_PROCESS - number of Closed enrollment guids to pull out of TDS 
 * DUMP_FILE_DIR = "/home/pryvalov/Documents/GroupEnrollment/comparison/" In case diff is found dumps both files to this folder
 * OLD_WORKFLOW = 'GroupSetupOld' and NEW_WORKFLOW = 'GroupSetup' - workflow id's to compare output
 */
import groovy.json.JsonSlurper
import groovy.util.logging.Slf4j
import groovy.xml.XmlUtil
import org.xmlunit.builder.DiffBuilder
import org.xmlunit.diff.ComparisonControllers

import javax.xml.datatype.DatatypeFactory

@Grab(group = 'org.xmlunit', module = 'xmlunit-core', version = '2.5.0')
@Grab(group='ch.qos.logback', module='logback-classic', version='[0.9.28,)')
@Slf4j
class XMLCompare {

    static final int RECORDS_TO_PROCESS = 5
    static final String DUMP_FILE_DIR = "./log/"
    static final JsonSlurper slurper = new JsonSlurper()
    static final XmlSlurper xmlSlurper = new XmlSlurper()
    public static final String OLD_WORKFLOW = 'GroupSetupOld'
    public static final String NEW_WORKFLOW = 'GroupSetup'

    private int succesfullCount = 0
    private List<String> corruptRecords = []
    
    private List<Long> oldTimings = []
    private List<Long> newTimings = []


    void compare() {
        def guids = slurper.parseText(searchClosedPeriodGuids(RECORDS_TO_PROCESS)).values.collect { it.guid }
        try {
            guids.each { performXmlComparison(it) }
        } finally {
            log.info("Successfully matched $succesfullCount records, \n Corrupt record guids: ${corruptRecords}  ")
            log.info("Old wf timings $oldTimings with average ${oldTimings.sum() / oldTimings.size()}")
            log.info("New wf timings $newTimings with average ${newTimings.sum() / newTimings.size()}")
        }
    }

    private void performXmlComparison(String guid) {
        
        def start = System.currentTimeMillis()
        def esbResponseOldWf = executeEnrollmentIntegration(guid, 'FinalGroupSetup', OLD_WORKFLOW)
        
        
        def expected = xmlSlurper.parseText(esbResponseOldWf)

        if (expected.Code != 200) {
            log.error("corrupt record or ESB failure, verify $guid")
            corruptRecords.add(guid)

            return
        }
        oldTimings.add(System.currentTimeMillis() - start)
        
        start = System.currentTimeMillis()
        def esbResponseNewWf = executeEnrollmentIntegration(guid, 'FinalGroupSetup', NEW_WORKFLOW)
        
        
        def actual = xmlSlurper.parseText(esbResponseNewWf)
        if (actual.Code != 200) {
            log.error("corrupt record or ESB failure, verify $guid")
            corruptRecords.add(guid)

            return
        }
        newTimings.add(System.currentTimeMillis() - start)
        def diff = DiffBuilder.compare(XmlUtil.serialize(expected.Payload.Employer))
                .withTest(XmlUtil.serialize(actual.Payload.Employer))
                .withComparisonController(ComparisonControllers.StopWhenDifferent)
                .build()

        if (diff.hasDifferences()) {
            new File("${DUMP_FILE_DIR}expected.xml").write expected
            new File("${DUMP_FILE_DIR}actual.xml").write actual

            throw new RuntimeException("Aborting execution due to found diff ${diff.toString()} \n Chech files dumped to $DUMP_FILE_DIR")

        } else {
            log.info "Processed $guid no differences found"
            succesfullCount++
        }
    }

    private String executeEnrollmentIntegration(guid, type, workflowId, carrierId = 'nixdev') {
        def timestamp = DatatypeFactory.newInstance().newXMLGregorianCalendar(new GregorianCalendar() {
            {
                setTime(new Date())
            }
        })
        def postToEsb = new URL("http://10.1.2.37:9000/integration/platform/workflowengine/rest/executeWorkflow").openConnection()
        def esbRequest = """
                <ns2:IntegrationRequest xmlns:ns2="http://www.connecture.com/integration/esb/platform" xmlns="http://www.connecture.com/integration/esb">
                  <InternalServiceRequest>
                    <RequestID>${UUID.randomUUID()}</RequestID>
                    <Version>1.0</Version>
                    <Timestamp>$timestamp</Timestamp>
                    <CarrierId>$carrierId</CarrierId>
                  </InternalServiceRequest>
                  <ns2:WorkflowId>$workflowId</ns2:WorkflowId>
                  <ns2:XmlPayload>
                    <Request xmlns="http://www.connecture.com/sgenrollment">
                     <GUID>$guid</GUID>
                     <type>$type</type>
                    </Request>
                </ns2:XmlPayload>
                </ns2:IntegrationRequest>
        """
        postToEsb.setRequestMethod("POST")
        postToEsb.setDoOutput(true)
        postToEsb.setRequestProperty("Content-Type", "application/xml")
        postToEsb.setRequestProperty("Authorization", "Basic cWFwbGF0Zm9ybXVzZXI6cWFwYXNzd29yZA==")
        postToEsb.getOutputStream().write(esbRequest.getBytes("UTF-8"))
        log.debug("executing $workflowId for $guid")
        def esbResponse = postToEsb.getInputStream().getText()
        log.debug("executed $workflowId for $guid")

        return esbResponse
    }
    private String searchClosedPeriodGuids(int recordsToRetrieve) {
        def post = new URL("http://cnxatlsgqa12.ct.com:9007/exemplar/groupEnrollment-v1/search?pagelen=$recordsToRetrieve").openConnection()
        def message = """
            {
                "searchQuery": {
                    "query": {
                        "match":{
                            "enrollmentPeriod:status.keyword": "Closed"
                        }
                    }
                },
                "responseLocations": {
                    "guid": "guid"
                }
            }
        """
        post.setRequestMethod("POST")
        post.setDoOutput(true)
        post.setRequestProperty("Content-Type", "application/json")
        post.setRequestProperty("Authorization", "Basic c3lzdGVtVXNlcjozY20ldDdYLWs3NWptQD9r")
        post.getOutputStream().write(message.getBytes("UTF-8"))

        return post.getInputStream().getText()
    }

}

new XMLCompare().compare()
