@Grapes([
        @Grab(group='ch.qos.logback', module='logback-classic', version='[0.9.28,)'),
        @Grab(group='com.microsoft.sqlserver', module='mssql-jdbc', version='6.2.1.jre8'),
        @GrabConfig(systemClassLoader = true)
])

import ch.qos.logback.classic.encoder.PatternLayoutEncoder
import ch.qos.logback.core.FileAppender
import groovy.json.JsonSlurper
import groovy.sql.Sql
import groovy.util.logging.Slf4j
import org.slf4j.LoggerFactory
import javax.xml.datatype.DatatypeFactory

@Slf4j
class DOMORecap0
{

    public static final DB_URI = 'jdbc:sqlserver://CNXBRKSGQA03.ct.com:49762;databaseName=EXEMPLAR_R7_8_x_QA1_Tran'
    public static final USER = 'universal_login'
    public static final PASSWORD = 'firefly'
    public static final DRIVER = 'com.microsoft.sqlserver.jdbc.SQLServerDriver'
    public static final String TEST_QUERY = '''
                    SELECT TOP 1 QT_QUOTE.QUOTE_ID, QT_QUOTE.GROUP_ID FROM QT_QUOTE
                      INNER JOIN QT_WORKFLOW_STATE ON QT_WORKFLOW_STATE.QUOTE_ID = QT_QUOTE.QUOTE_ID
                      INNER JOIN GR_GROUP ON QT_QUOTE.GROUP_ID = GR_GROUP.GROUP_ID
                    WHERE  GR_GROUP.GROUP_XREF_TYPE_ID = 3 AND 
                    (GR_GROUP.GROUP_TYPE_KEY = 'LG' AND QT_WORKFLOW_STATE.WORKFLOW_STATE_TYPE_ID IN (9, 21)) 
                    OR 
                    (GR_GROUP.GROUP_TYPE_KEY = 'SG' AND QT_WORKFLOW_STATE.WORKFLOW_STATE_TYPE_ID = 9)
                    ORDER BY QT_QUOTE.QUOTE_ID DESC
                    '''

    public static final String WORK_QUERY = '''
                    SELECT QT_QUOTE.QUOTE_ID, QT_QUOTE.GROUP_ID FROM QT_QUOTE
                      INNER JOIN QT_WORKFLOW_STATE ON QT_WORKFLOW_STATE.QUOTE_ID = QT_QUOTE.QUOTE_ID
                      INNER JOIN GR_GROUP ON QT_QUOTE.GROUP_ID = GR_GROUP.GROUP_ID
                    WHERE  GR_GROUP.GROUP_XREF_TYPE_ID = 3 AND 
                    (GR_GROUP.GROUP_TYPE_KEY = 'LG' AND QT_WORKFLOW_STATE.WORKFLOW_STATE_TYPE_ID IN (9, 21)) 
                    OR 
                    (GR_GROUP.GROUP_TYPE_KEY = 'SG' AND QT_WORKFLOW_STATE.WORKFLOW_STATE_TYPE_ID = 9)
                    '''

    static {
        new FileAppender().with {
            name = 'file appender'
            file = '../log/groovy.log'
            context = LoggerFactory.getILoggerFactory()
            encoder = new PatternLayoutEncoder().with {
                context = LoggerFactory.getILoggerFactory()
                pattern = "%date{HH:mm:ss.SSS} [%thread] %-5level %logger{35} - %msg%n"
                start()
                it
            }
            start()
            log.addAppender(it)
        }
    }

    Object run() {

        log.info "Establising connection to $DB_URI"

        LinkedHashMap acceptedQuotesMap = findAcceptedPandaQuotes()

        log.info "Found ${acceptedQuotesMap.size()} accepted quotes in the database."

        log.info "Performing TDS elasticsearch for associated records..."
        String searchResponse = submitTdsSearch(acceptedQuotesMap)

        def parsedResponse = new JsonSlurper().parseText(searchResponse)

        log.info "TDS responded with ${parsedResponse.size} results."

        //filter not-matching quoteId - groupId guids, probably from other IA
        def guids = parsedResponse.values.findAll
        { val -> acceptedQuotesMap.get(val.quoteId) == val.groupId }.collect { it.guid }

        guids.removeAll([null]) //sometimes elasticsearch responds with null guid for some reason

        log.info "Found ${guids.size()} matching GUID records."

        guids.each { guid ->
            def esbResponse = submitToEsb(guid)


            def failedRequests = 0

            def parsedEsbResponse = new XmlSlurper().parseText(esbResponse)
            if (parsedEsbResponse.Code == 200) {
                log.info "GUID: $guid was submitted to the DOMO Service."
            } else {
                log.warn esbResponse
                if (++failedRequests > 10) {
                    log.error 'Terminating after 10 failed requests'
                    throw new RuntimeException('Terminating after 10 failed requests')
                }
            }
        }
    }

    private LinkedHashMap findAcceptedPandaQuotes() {
        def acceptedQuotesMap = [:]
        def sql = Sql.newInstance(DB_URI, USER, PASSWORD, DRIVER)

        sql.query(TEST_QUERY) { resultSet ->
            while (resultSet.next()) {
                def quoteId = resultSet.getInt(1)
                def groupId = resultSet.getInt(2)
                acceptedQuotesMap.put(quoteId, groupId)
            }
        }
        sql.close()

        return acceptedQuotesMap
    }

    private String submitTdsSearch(LinkedHashMap acceptedQuotesMap, page = 1) {
        //def post = new URL("http://localhost:9007/exemplar/groupEnrollment-v1/search").openConnection()
        def post = new URL("http://cnxatlsgqa12.ct.com:9007/exemplar/groupEnrollment-v1/search?page=$page").openConnection()
        def message = """
                {
                    "searchQuery": {
                        "query": {
                            "terms":{
                                "quoteData:id.keyword": ${acceptedQuotesMap.keySet()}
                            }
                        }
                    },
                    "responseLocations": {
                        "guid": "guid",
                        "groupId": "groupData.id",
                        "quoteId": "quoteData.id",
                        "type": "groupData.type"
                    }
                }
        """
        post.setRequestMethod("POST")
        post.setDoOutput(true)
        post.setRequestProperty("Content-Type", "application/json")
        post.setRequestProperty("Authorization", "Basic c3lzdGVtVXNlcjozY20ldDdYLWs3NWptQD9r")
        post.getOutputStream().write(message.getBytes("UTF-8"))

        def outputText = post.getInputStream().getText()

        return outputText
    }

    private String submitToEsb(guid) {
        def timestamp = DatatypeFactory.newInstance().newXMLGregorianCalendar(new GregorianCalendar() {
            {
                setTime(new Date())
            }
        })
        def postToEsb = new URL("http://10.1.2.37:9000/integration/platform/workflowengine/rest/executeWorkflow").openConnection()
        def esbRequest = """
                <ns2:IntegrationRequest xmlns:ns2="http://www.connecture.com/integration/esb/platform" xmlns="http://www.connecture.com/integration/esb">
                  <InternalServiceRequest>
                    <RequestID>${UUID.randomUUID()}</RequestID>
                    <Version>1.0</Version>
                    <Timestamp>$timestamp</Timestamp>
                    <CarrierId>FlexCare</CarrierId>
                  </InternalServiceRequest>
                  <ns2:WorkflowId>DOMOReport</ns2:WorkflowId>
                  <ns2:XmlPayload>
                    <Request xmlns="http://www.connecture.com/sgenrollment">
                     <GUID>$guid</GUID>
                    </Request>
                </ns2:XmlPayload>
                </ns2:IntegrationRequest>
        """
        postToEsb.setRequestMethod("POST")
        postToEsb.setDoOutput(true)
        postToEsb.setRequestProperty("Content-Type", "application/xml")
        postToEsb.setRequestProperty("Authorization", "Basic cWFwbGF0Zm9ybXVzZXI6cWFwYXNzd29yZA==")
        postToEsb.getOutputStream().write(esbRequest.getBytes("UTF-8"))

        def esbResponse = postToEsb.getInputStream().getText()

        return esbResponse
    }
}
def runner = new DOMORecap0()
runner.run()
